package com.yunfinal.api.tpl;

import com.jfinal.config.*;
import com.jfinal.kit.Prop;
import com.jfinal.kit.PropKit;
import com.jfinal.plugin.activerecord.ActiveRecordPlugin;
import com.jfinal.plugin.activerecord.dialect.Sqlite3Dialect;
import com.jfinal.plugin.druid.DruidPlugin;
import com.jfinal.template.Engine;
import com.yunfinal.api.tpl.json.ApiController;
import com.yunfinal.api.tpl.json.ApiHandler;
import com.yunfinal.api.tpl.json.ApiJsonInterceptor;

/**
 * com.yunfinal.api.tpl.ApiConfig
 */
public class ApiConfig extends JFinalConfig {
    private Prop p = PropKit.append("apiConfig.txt").appendIfExists("apiConfig_pro.txt");

    @Override
    public void configConstant(Constants me) {
        me.setDevMode(p.getBoolean("devMode", false));
    }

    @Override
    public void configRoute(Routes me) {
        me.add("/api", ApiController.class);
    }

    @Override
    public void configEngine(Engine me) {
        EngineConfig.config(new Engine("API_SQL"));
    }

    @Override
    public void configPlugin(Plugins me) {
        //方便体验测试，这里放sqlite数据库
        String jdbcUrl = p.get("jdbcUrl");
        DruidPlugin dp = new DruidPlugin(jdbcUrl, p.get("user"), p.get("password"));
        ActiveRecordPlugin arp = new ActiveRecordPlugin(dp);
        me.add(dp).add(arp);
        if (jdbcUrl.toLowerCase().contains("jdbc:sqlite")){
            dp.set(1, 1, 1);
            dp.setDriverClass("org.sqlite.JDBC");
            arp.setDialect(new Sqlite3Dialect());
        }
    }

    @Override
    public void configInterceptor(Interceptors me) {
        me.add(new ApiJsonInterceptor());
    }

    @Override
    public void configHandler(Handlers me) {
        me.add(new ApiHandler());
    }
}