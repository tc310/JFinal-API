package com.yunfinal.api.tpl.json;

import com.jfinal.aop.Interceptor;
import com.jfinal.aop.Invocation;
import com.jfinal.core.JFinal;
import com.jfinal.kit.Kv;
import com.jfinal.log.Log;

public class ApiJsonInterceptor implements Interceptor {
    private static final Log log = Log.getLog(ApiJsonInterceptor.class);

    @Override
    public void intercept(Invocation inv) {
        try {
            inv.invoke();
        } catch (Exception e) {
            if (JFinal.me().getConstants().getDevMode()) {
                log.error("异常", e);
            }
            inv.getController().renderJson(Kv.by("code", 500).set("msg", "未知异常错误，请联系管理员"));
        }
    }
}
